# clone_quizlet

quizlet clone coding 프로젝트 입니다.

---
### [기술선정]
- FrontEnd: VueJS
- BackEnd: Express
- DB: Mongo DB
- Express-DB 연결은 Sequelize 활용 예정
- 형상관리: gitlab
- CICD: gitlab runner

- #### application
    - Infra: S3, Lambda, Document DB
- #### Deploy target
    - FrontEnd -> AWS S3
    - BackEnd -> AWS Lambda (도전 예정)
    - DB -> AWS Document DB (mongo DB 3.6 호환됨)